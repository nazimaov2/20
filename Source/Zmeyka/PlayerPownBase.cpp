// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPownBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "ZmeykaBase.h"


// Sets default values
APlayerPownBase::APlayerPownBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;
}

// Called when the game starts or when spawned
void APlayerPownBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));
	CreateZmeykaActor();
}

// Called every frame
void APlayerPownBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerPownBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void APlayerPownBase::CreateZmeykaActor()
{
	ZmeykaActor = GetWorld()->SpawnActor<AZmeykaBase>(ZmeykaActorClass, FTransform());
}

