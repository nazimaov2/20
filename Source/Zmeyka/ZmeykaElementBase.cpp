// Fill out your copyright notice in the Description page of Project Settings.


#include "ZmeykaElementBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
// Sets default values
AZmeykaElementBase::AZmeykaElementBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
}

// Called when the game starts or when spawned
void AZmeykaElementBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AZmeykaElementBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

