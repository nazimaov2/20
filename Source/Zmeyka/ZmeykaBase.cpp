// Fill out your copyright notice in the Description page of Project Settings.


#include "ZmeykaBase.h"
#include "ZmeykaElementBase.h"

// Sets default values
AZmeykaBase::AZmeykaBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
}

// Called when the game starts or when spawned
void AZmeykaBase::BeginPlay()
{
	Super::BeginPlay();
	AddZmeykaElement(4);
	}

// Called every frame
void AZmeykaBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AZmeykaBase::AddZmeykaElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i)
	{
		FVector NewLocation(ZmeykaElements.Num() * ElementSize, 0, 0);
		FTransform NewTransform(NewLocation);
		auto NewZmeykaElem = GetWorld()->SpawnActor<AZmeykaElementBase>(ZmeykaElementClass, NewTransform);
		ZmeykaElements.Add(NewZmeykaElem);
	}

}

