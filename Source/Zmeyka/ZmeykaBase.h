// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ZmeykaBase.generated.h"

class AZmeykaElementBase;

UCLASS()
class ZMEYKA_API AZmeykaBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AZmeykaBase();

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AZmeykaElementBase> ZmeykaElementClass;

	UPROPERTY(EditDefaultsOnly)
		float ElementSize;

	UPROPERTY()
		TArray<AZmeykaElementBase> ZmeykaElements;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void AddZmeykaElement( int ElementsNum = 1);
};
